package com.demo.springboot;

import org.springframework.stereotype.Service;

@Service
public class IdentificationNumber {
    private final int NUMBER_LENGTH = 11;
    private final byte[] PESEL = new byte[11];

    public boolean isValid(String peselNumber) {
        if (peselNumber.length() != NUMBER_LENGTH) {
            return false;
        } else {
            for (int i = 0; i < 11; i++) {
                PESEL[i] = Byte.parseByte(peselNumber.substring(i, i + 1));
            }
            return countCheck();
        }
    }


    private boolean countCheck() {
        int countSum = 1 * PESEL[0]+
                  3 * PESEL[1]+
                  7 * PESEL[2]+
                  9 * PESEL[3]+
                  1 * PESEL[4]+
                  3 * PESEL[5]+
                  7 * PESEL[6]+
                  9 * PESEL[7]+
                  1 * PESEL[8]+
                  3 * PESEL[9];
        countSum %= 10;
        countSum = 10 - countSum;
        countSum %= 10;

        return countSum == PESEL[10];
    }
}


